const servidorSalaGeneral = io('http://localhost:3002/chat');

let nickName;

servidorSalaGeneral.on('connect', () => console.log('Se conecto al socket....'));
servidorSalaGeneral.on('chat', (respuestaChat) => console.log(respuestaChat)); // para registrar nick
servidorSalaGeneral.on('salir', (respChat) => console.log());// caundo alguien abandpna la sala
servidorSalaGeneral.on('listarMensajes', mensajes => mensajes.forEach(mensaje => console.log(mensaje)));
servidorSalaGeneral.on('unirse-room', res => console.log(res));


function ingresarNick(nick) {
    nickName = nick;
    servidorSalaGeneral.emit('registrar-nick', nick, (respuesta) => console.log(respuesta))

}

function enviarMensaje(mensaje) {
    servidorSalaGeneral.emit('mandar-mensaje', `${nickName}:  ${mensaje}`, (res) => console.log(res))// aqui puedo decir su     mensaje ha sido enviado

}

function enviarMensajeSala(mensaje, sala) {
    servidorSalaGeneral.emit('mandar-mensaje-sala', {
        "mensaje": `${nickName}:  ${mensaje}`,
        "sala": sala
    }, (res) => console.log(res))// aqui puedo decir su mensaje ha sido enviado

}

function desplegarMensajes() {
    servidorSalaGeneral.emit('listar', '', () => {
    })

}

function desconectarse(room) {
    servidorSalaGeneral.emit('dejar-room', room, (res) => {
        console.log(res)
    })
}

function unirseARoom(room) {
    servidorSalaGeneral.emit('unirse-room', room, (res) => {
        console.log(res)
    })
}

function unirseRoomIsra(nick) {
    servidorSalaGeneral.emit('unirse-room-isra', nick, r => console.log(r))

}

function crearRooms() {
    servidorSalaGeneral.emit('crear-rooms', '', res => console.log(res));

}


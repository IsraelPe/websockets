import {BadRequestException, Body, Controller, Get, Param, Post, Query, Session} from '@nestjs/common';
import {AppService} from './app.service';

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get('registrar-nick/:nick')
    registrarNick(@Param('nick')nick) {
        return this.appService.registrarNick(nick);
    }

    @Post('enviar-mensaje/:mensaje')
    mandarMensaje(@Param('mensaje')mensaje) {
        return this.appService.enviarMensaje(mensaje);
    }

    @Get('/listar/mensajes')
    listarMensajes() {
        return this.appService.listarMansajes();
    }

    @Post('login')
    login(@Body()credenciales: CredencialesInterface, @Session()session) {
        if (credenciales) {
            session.nombre = credenciales.nombre;
            session.pass = credenciales.pass;
            console.log('Sesion:', session);
            return session;
        } else {
            throw new BadRequestException('no envia credenciales');
        }
    }

    @Get('logout')
    salir(@Session()session) {
        console.log('sesion a destruir: ', session);
        session.nombre = undefined;
        session.pass = undefined;
        session.destroy();
        console.log(session);
        return 'ok';
    }

    @Get('obtener-sesion')
    obternerSesion(@Session() session) {
        console.log(session);
        return session;
    }
}

interface CredencialesInterface {
    nombre?: string;
    pass?: string;
}

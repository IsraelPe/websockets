import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WebsocketsModule } from './websockets/websockets.module';
import { OperadoraGateway } from './websockets/operadora.gateway';
import { ClienteGateway } from './websockets/cliente.gateway';

@Module({
  imports: [WebsocketsModule],
  controllers: [AppController],
  providers: [AppService, OperadoraGateway, ClienteGateway],
})
export class AppModule {}

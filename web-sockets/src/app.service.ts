import {Injectable} from '@nestjs/common';
import * as io from 'socket.io-client';

@Injectable()
export class AppService {
    servidorWS;
    nick;

    constructor() {
        this.iniciarConexionWS();

    }

    iniciarConexionWS() {
        this.servidorWS = io('http://localhost:3002/chat');
        this.servidorWS.on('connect', () => console.log('socket conectado al servicio'));
    }

    registrarNick(nick: string): Promise<any> {
        this.nick = nick;
        return new Promise((resolve, reject) => {
            this.servidorWS.emit('registrar-nick', nick, (res) => {
                resolve(res);
            });
        });
    }

    enviarMensaje(mensaje: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.servidorWS.emit('mandar-mensaje', `${this.nick} ${mensaje}`, (res) => {
                resolve(res);
            });
        });
    }

    listarMansajes(): Promise<any> {
        return new Promise((resolve, reject) => {
            resolve(this.servidorWS.emit('listar'));
        });
    }
}
import {NestFactory} from '@nestjs/core';
import {AppModule} from './app.module';
import {RedisIoAdapter} from "./redis-adapter/redis.adapter-ws";
import * as express from 'express';
//const  session = require('express-session');
import * as session from 'express-session';

const FileStore = require('session-file-store')(session);
const cookieParser = require('cookie-parser');

async function bootstrap() {
    const app = await NestFactory.create(AppModule);
    app.use(session(
        {
            name: 'session-storage', // nombre para identificar a la session
            secret: 'nekokey',
            resave: false,
            saveUninitialized: true,
            store: new FileStore(), // donde se va a almacenar la sesison
            cookie: {secure: false},
        },
    ));

    app.useWebSocketAdapter(new RedisIoAdapter(app));
    app.use(express.static('publico'));
    await app.listen(3000);
}

bootstrap();

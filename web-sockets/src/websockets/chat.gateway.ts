import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
} from '@nestjs/websockets';

@WebSocketGateway(3002, {namespace: 'chat'})
export class ChatGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    mensajes = [];

    afterInit(server: any): any {
        console.log('chat iniciado');
    }

    handleConnection(client: any, ...args: any[]): any {
        console.log('el usuario se ha conectado', client.id);
    }

    handleDisconnect(client: any): any {
        console.log('el usuario se ha desconectado', client.id);
    }

    @SubscribeMessage('crear-rooms')
    crearRooms(cliente: any, mensaje: any) {
        cliente.join('room1');
        cliente.join('room2');
        console.log(cliente.eventNames());
        const llaves = Object.keys(cliente.adapter.rooms)
            .filter(valor => {
                return valor.substring(0, 1) !== '/';
            });
        console.log(llaves);
        return `rooms: ${llaves}`;
    }

    @SubscribeMessage('registrar-nick')
    handleMessage(client: any, nick: any): string {
        client.broadcast.emit('chat', `Se ha conectado: ${nick}`);
        return `Bienvenido:  ${nick}`;
    }

    @SubscribeMessage('chat')
    chat(client: any, data: any) {
        return data;
    }

    @SubscribeMessage('mandar-mensaje')
    mandarMensaje(client: any, mensaje: any) {
        this.mensajes.push(mensaje);
        client.broadcast.emit('chat', mensaje);
        return mensaje;
    }

    @SubscribeMessage('mandar-mensaje-sala')
    mandarMensajeSala(client: any, data: any) {
        this.mensajes.push(data.mensaje);
        client.to(data.sala).emit('chat', data.mensaje);
        return data.mensaje;
    }


    @SubscribeMessage('listar')
    listarHistorialMensajes(cliente: any) {
        console.log(this.mensajes);
        cliente.emit('chat', this.mensajes);
    }

    @SubscribeMessage('unirse-room')
    unirseRoom(client: any, nombreRoom: any) {
        client.join(nombreRoom);
        client.to(nombreRoom).emit('chat', `Alguien se ha unido a la sala ${nombreRoom}`);
        return `Te uniste a la sala ${nombreRoom}`;
    }

    @SubscribeMessage('dejar-room')
    dejarRoom(client: any, nombreRoom: any) {
        client.leave(nombreRoom);
        client.to(nombreRoom).emit('chat', `alguien abandono la sala ${nombreRoom}`);
        return `Dejaste la sala ${nombreRoom}`;
    }

    @SubscribeMessage('unirse-room-isra')
    unirseIsra(client: any, nick: any) {
        client.join('Israel');
        client.to('Israel').emit('chat', `${nick} se ha unido a la sala `);
        return `${nick} te uniste a la sala Israel `;
    }
}

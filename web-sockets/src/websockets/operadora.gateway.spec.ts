import { Test, TestingModule } from '@nestjs/testing';
import { OperadoraGateway } from './operadora.gateway';

describe('OperadoraGateway', () => {
  let gateway: OperadoraGateway;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OperadoraGateway],
    }).compile();

    gateway = module.get<OperadoraGateway>(OperadoraGateway);
  });

  it('should be defined', () => {
    expect(gateway).toBeDefined();
  });
});

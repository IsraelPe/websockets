import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway
} from '@nestjs/websockets';

@WebSocketGateway(3002, {namespace: 'operadora'})
export class OperadoraGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
    /*OnGatewayInit: solamente para saber si el socket esta listo*/
    afterInit(server: any) {
        console.log('la operadora emprzo a escuchar')
    }

    handleConnection(client: any, ...args): any {
      console.log('se conecto el cliente ', client.id);
    }

    handleDisconnect(client: any): any {
      console.log('se desconecto el clinte ', client.id);
    }

  //esto es una sala
    @SubscribeMessage('saludar')
    getHello(client: any, data: any): string {
        // toda la logica.
        // si elijo broadcast o unicast
        //omportar otros servicios para hace culauiw operacion.
        // aqui elijo el tipo de respuesta que puedo dar.
        // aqui son solamente evento.
        client.broadcast.emit('evento', data);

        return 'Hello world!'; // solo para el cliente :: su mensaje ha sido enviado
    }

    @SubscribeMessage('adios')
    decirAdios(client: any, data: any): string {
        return 'topamos';
    }

}


// payload siempre es una data. es todo lo que vayamos a mandar
